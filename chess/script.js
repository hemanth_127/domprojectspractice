var div = document.querySelector('.row')

function highlightcolor (r, c) {
  sq = document.querySelectorAll('.square')
  sq.forEach(sqrt => {
    r = parseInt(r)
    c = parseInt(c)
    row = parseInt(sqrt.getAttribute('row_index'))
    col = parseInt(sqrt.getAttribute('col_index'))
    
    // sqrt.classList.remove('hi')
    //plus line for rows and columns
    // if (r === row || c == col) {
    //   sqrt.classList.toggle('hi')
    // }

    if (Math.abs(row - r) === Math.abs(col - c) || (row === r && col === c)) {
      sqrt.classList.toggle('hi')
    }
  })
}

function create () {
  var page = document.getElementsByClassName('container')[0]
  for (let i = 0; i < 8; i++) {
    subdiv = document.createElement('div')
    subdiv.setAttribute('class', 'row')
    for (let j = 0; j < 8; j++) {
      div = document.createElement('div')
      div.classList.add('square')
      div.setAttribute('row_index', i)
      div.setAttribute('col_index', j)

      // div.addEventListener('click', e => {
      //   trow = e.target.getAttribute('row_index')
      //   tcol = e.target.getAttribute('col_index')

      //   console.log(e.target)
      //   highlightcolor(trow, tcol)
      // })

      subdiv.appendChild(div)
    }
    page.appendChild(subdiv)
  }
  console.log(page)
}

create()

container = document.querySelector('.container')
// console.log(container);
container.addEventListener('click', function (e) {
  console.log(e.target)
  if (e.target.classList.contains('square')) {
    var trow = e.target.getAttribute('row_index')
    var tcol = e.target.getAttribute('col_index')
    highlightcolor(trow, tcol)
  }
})
